# commons.garden

## CI/CD

The project is configured to use GitLab CI/CD. The CI/CD pipeline is defined in the `.gitlab-ci.yml` file.
The pipeline is configured to run the following stages:
- 'pages': builds the Hugo static site 
- 'pages:deploy': deploys the static site to the GitLab Pages server

Site url: https://gpicron.gitlab.io/commons-garden-site/